﻿using System;
using Android.App;
using Android.Content.PM;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;

namespace FKBurguers.Droid
{
  [Activity(Label = "FKBurguers", Icon = "@drawable/icon", Theme = "@style/MainTheme", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
  public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
  {
    protected override void OnCreate(Bundle bundle)
    {
      TabLayoutResource = Resource.Layout.Tabbar;
      ToolbarResource = Resource.Layout.Toolbar;

      base.OnCreate(bundle);

      global::Xamarin.Forms.Forms.Init(this, bundle);
			ImageCircle.Forms.Plugin.Droid.ImageCircleRenderer.Init();
      LoadApplication(new App());

      // Efetua o login no Facebook.
      Microsoft.WindowsAzure.MobileServices.CurrentPlatform.Init();
    }
  }
}

