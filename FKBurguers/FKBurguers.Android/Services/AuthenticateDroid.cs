﻿using System;
using FKBurguers.Interfaces;
using FKBurguers.Droid.Services;
using Microsoft.WindowsAzure.MobileServices;
using System.Threading.Tasks;

[assembly: Xamarin.Forms.Dependency(typeof(AuthenticateDroid))]
namespace FKBurguers.Droid.Services
{
  public class AuthenticateDroid : IAuthenticate
  {
    public async Task<MobileServiceUser> Authenticate(MobileServiceClient client, MobileServiceAuthenticationProvider provider)
    {
      try
      {
        return await client.LoginAsync(Xamarin.Forms.Forms.Context, provider);
      }
      catch (Exception ex)
      {
        return null;
      }
    }
  }
}