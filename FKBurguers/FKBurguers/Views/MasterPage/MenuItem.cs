﻿using System;
namespace FKBurguers.Views.MasterPage
{
  public class MenuItem
  {
    public string Title { get; set; }

    public string FontCode { get; set; }

    public Type TargetType { get; set; }

    public object[] parametros { get; set; }

  }
}
