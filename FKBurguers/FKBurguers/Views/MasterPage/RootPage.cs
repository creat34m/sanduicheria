﻿using System;
using Xamarin.Forms;

namespace FKBurguers.Views
{
  public class RootPage : MasterDetailPage
  {
    public string UserName { get; set; }
    public MenuPage MenuPage { get; set; }

    public RootPage(string userName)
    {
      UserName = userName;
    }

    public RootPage()
    {
      this.MenuPage = new MenuPage();
      this.MenuPage.MenuListView.ItemSelected += (sender, e) => NavigateTo(e.SelectedItem as MasterPage.MenuItem);

      this.Master = this.MenuPage;
      this.Detail = CreateNavigationPage(new MainPage());
    }

    public NavigationPage CreateNavigationPage(Page pag)
    {
      var nbar = new NavigationPage(pag)
      {
        BarBackgroundColor = Color.Gold,
        BarTextColor = Color.Black
      };

      return nbar;
    }

    public void NavigateTo(MasterPage.MenuItem menu)
    {
      if (menu == null)
        return;

      Page displayPage = (Page)Activator.CreateInstance(menu.TargetType, menu.parametros);
      this.Detail = CreateNavigationPage(displayPage);

      this.MenuPage.MenuListView.SelectedItem = null;
      this.IsPresented = false;
    }

  }
}
