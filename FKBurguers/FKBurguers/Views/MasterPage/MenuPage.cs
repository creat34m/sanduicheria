﻿using System;
using FKBurguers.Styles;
using FKBurguers.Views.MasterPage;
using ImageCircle.Forms.Plugin.Abstractions;
using Xamarin.Forms;

namespace FKBurguers.Views
{
    public class MenuPage : ContentPage
    {
        public ListView MenuListView { get; set; }

        public MenuPage()
        {
            this.ToolbarItems.Clear();

            this.Title = "Menu";
            this.BackgroundColor = Color.Gold;
            this.MenuListView = new ListView()
            {
                ItemsSource = new MenuListData(),

                VerticalOptions = LayoutOptions.FillAndExpand,
                //this.MenuListView.BackgroundColor = Color.Transparent;

                //this.MenuListView.SeparatorColor = Color.Transparent;
                ItemTemplate = new DataTemplate(typeof(MenuCell))
            };

            this.MenuListView.ItemSelected += (o, e) =>
            {
                this.MenuListView.SelectedItem = null;
            };

            var backgroundImage = new Image()
            {
                Source = "fast_food.png",
                Aspect = Aspect.AspectFill,
                IsOpaque = true,
                Opacity = 0.8,
            };

            var shader = new BoxView()
            {
                Color = Color.Black.MultiplyAlpha(.2)
            };

            var shader5 = new BoxView()
            {
                Color = Color.Black.MultiplyAlpha(.5)
            };

            var face = new CircleImage()
            {
                Aspect = Aspect.AspectFit,
                Source = "icon.png"
            };

            StackLayout text = new StackLayout()
            {
                Children =
                {
                    new Label
                    {
                        Text = "Bem Vindo",
                        TextColor = Color.White,
                        FontAttributes= FontAttributes.Bold
                    }
                }
            };
            RelativeLayout relativeLayout = new RelativeLayout();
            relativeLayout.Children.Add
            (
                backgroundImage,
                Constraint.Constant(0),
                Constraint.Constant(0),
                Constraint.RelativeToParent((parent) =>
               {
                   return parent.Width;
               }),
                Constraint.RelativeToParent((parent) =>
               {
                   return parent.Height * .3;
               })
            );

            relativeLayout.Children.Add
            (
                shader,
                Constraint.Constant(0),
                Constraint.Constant(0),
                Constraint.RelativeToParent((parent) =>
               {
                   return parent.Width;
               }),
                Constraint.RelativeToParent((parent) =>
               {
                   return parent.Height * .3;
               })
            );

            relativeLayout.Children.Add
            (
                shader5,
                Constraint.Constant(0),
                Constraint.Constant(0),
                Constraint.RelativeToParent((parent) =>
               {
                   return parent.Width;
               }),
                Constraint.RelativeToParent((parent) =>
               {
                   return 0;
               })
            );
            relativeLayout.Children.Add
            (
                face,
                Constraint.Constant(5),
                Constraint.Constant(25),
                Constraint.RelativeToParent((parent) =>
               {
                   return parent.Width * .15;
               }),
                Constraint.RelativeToParent((parent) =>
               {
                   return parent.Width * .15;
               })
            );
            relativeLayout.Children.Add
            (
                text,
                Constraint.Constant(60),
                Constraint.Constant(25),
                Constraint.RelativeToParent((parent) =>
               {
                   return parent.Width;
               }),
                Constraint.RelativeToParent((parent) =>
               {
                   return parent.Width;
               })
            );

            relativeLayout.Children.Add
            (
                MenuListView,
                Constraint.RelativeToParent((parent) =>
               {
                   return parent.Width * .03;
               }),

                Constraint.RelativeToParent((parent) =>
               {
                   return (parent.Height * .35);
               }),
                Constraint.RelativeToParent((parent) =>
               {
                   return parent.Width;
               }),
                Constraint.RelativeToParent((parent) =>
               {
                   return parent.Height;
               })

            );

            Content = relativeLayout;
        }

    }
}
