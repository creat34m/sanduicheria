﻿using System;
using System.Collections.Generic;
using FKBurguers.Styles;

namespace FKBurguers.Views.MasterPage
{
    public class MenuListData : List<MenuItem>
    {
        public MenuListData()
        {
            this.Add(new MenuItem()
            {
                Title = "Login",
                FontCode = Styles.FontAwesomeIcons.SigIn,
                TargetType = typeof(LoginPage)
            });

            this.Add(new MenuItem()
            {
                Title = "Cardápio",
                FontCode = Styles.FontAwesomeIcons.List,
                TargetType = typeof(CardapioPage),
            });

            this.Add(new MenuItem()
            {
                Title = "Fale Conosco",
                FontCode = Styles.FontAwesomeIcons.Phone,
                TargetType = typeof(FaleConoscoPage)
            });
        }
    }
}
