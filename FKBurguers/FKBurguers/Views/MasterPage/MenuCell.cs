﻿using System;
using FKBurguers.Styles;
using Xamarin.Forms;

namespace FKBurguers.Views.MasterPage
{
  public class MenuCell : ViewCell
  {
    public MenuCell() : base()
    {
      var icon = new Label();
      icon.FontFamily = Fonts.FontAwesome;
      icon.FontSize = 30;
      icon.TextColor = Color.Black;

      StackLayout cellWrapper = new StackLayout();
      StackLayout horizontalLayout = new StackLayout { };
      Label left = new Label();


      left.SetBinding<MenuItem>(Label.TextProperty, i => i.Title);
      icon.SetBinding<MenuItem>(Label.TextProperty, i => i.FontCode);


      horizontalLayout.Orientation = StackOrientation.Horizontal;
      cellWrapper.VerticalOptions = LayoutOptions.Center;
      cellWrapper.Padding = new Thickness(10, 0, 0, 0);
      left.HorizontalOptions = LayoutOptions.Center;

      left.TextColor = Color.Gray;
      left.FontAttributes = FontAttributes.Bold;

      cellWrapper.Children.Add(left);

      horizontalLayout.VerticalOptions = LayoutOptions.Center;
      horizontalLayout.Children.Add(icon);
      horizontalLayout.Children.Add(cellWrapper);
      horizontalLayout.Padding = new Thickness(10, 0, 0, 0);

      View = horizontalLayout;

    }
  }
}
