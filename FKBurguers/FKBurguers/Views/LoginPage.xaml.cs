﻿using FKBurguers.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace FKBurguers.Views
{
  [XamlCompilation(XamlCompilationOptions.Compile)]
  public partial class LoginPage : ContentPage
  {
    private AzureService azureService = new AzureService();

    public LoginPage()
    {
      this.Title = "Login";

      InitializeComponent();

      FacebookLoginButton.Clicked += async (sender, args) =>
      {
        var user = await azureService.LoginAsync();

        if (user != null)
          NavigateAuthenticatedToMain(user?.UserId);
        else
          return;
      };
    }

    public async void NavigateAuthenticatedToMain(string userID)
    {
      var page = new RootPage(userID);
      await this.Navigation.PushAsync(page);
    }
  }
}