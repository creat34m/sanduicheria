﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;
using System.Collections.ObjectModel;
using FKBurguers.Models;
using static FKBurguers.Models.Cardapio;
using System.Reflection;
using System.Linq;

namespace FKBurguers.Views
{
    public partial class CardapioPage : ContentPage
    {
        public ObservableCollection<AgrupamentoCardapioModel> CardapioCollection { get; set; }

        public CardapioPage()
        {
            this.Title = "Cardápio";
            InitializeComponent();

            var tradicionaisGrupo = new AgrupamentoCardapioModel() { NomeCompleto = "Padrãzão", NomeAbreviado = ":)" };
            tradicionaisGrupo.Add(new Cardapio { Nome = "Hamburguer", Imagem = "Hamburger.png", Preco = 5, Descricao = "Simplizão é com S ou com Z?" });
            tradicionaisGrupo.Add(new Cardapio { Nome = "Cheese Burguer", Preco = 6, Descricao = "Esse tem queijo", Imagem = "CheeseBacon.png" });
            tradicionaisGrupo.Add(new Cardapio { Nome = "Cheese Bacon", Preco = 7, Descricao = "Hamburguer, queijo e bacon, muito bacon com pão de gergilin", Imagem = "CheeseBacon.png" });
            tradicionaisGrupo.Add(new Cardapio { Nome = "X Egg Bacon", Preco = 8, Descricao = "Hamburguer, queijo e bacon, muito bacon, só que esse tem ovo o de cima não :) e o pão de gergilin também", Imagem = "CheeseEggBacon.png" });
            tradicionaisGrupo.Add(new Cardapio { Nome = "X EggBurguer", Preco = 7, Descricao = $"Então não gosta de bacon né?{Environment.NewLine}Qual seu problema??? :/", Imagem = "CheeseEggBurguer.png" });

            var vegetarianoGrupo = new AgrupamentoCardapioModel() { NomeCompleto = "Vegetariano", NomeAbreviado = ":(" };
            vegetarianoGrupo.Add(new Cardapio { Nome = "X Salada", Preco = 6.5, Descricao = "Salada e queijo combinam sim...", Imagem = "CheeseSalada.png" });

            var premiumGrupo = new AgrupamentoCardapioModel() { NomeCompleto = "Premium modafoca", NomeAbreviado = "=)" };
            premiumGrupo.Add(new Cardapio { Nome = "Laçador", Preco = 10, Descricao = $"Esse é para dar um nó na sua fome ;P{Environment.NewLine}#BaduntsssssssBurguer", Imagem = "Lacador.png" });
            premiumGrupo.Add(new Cardapio { Nome = "Picanha Burguer", Preco = 12.5, Descricao = "Hamburguer de picanha de verdade (será??? vc acredita nos outros, pq não acreditaria nesse?)", Imagem = "PicanhaBurguer.png" });
            premiumGrupo.Add(new Cardapio { Nome = "Desafio", Preco = 15, Descricao = "É aqui que separamos os meninos dos homens! Você é fodão mesmo?", Imagem = "Desafio.png" });
            premiumGrupo.Add(new Cardapio
            {
                Nome = "Special",
                Preco = 15,
                Descricao = $"A especialidade da casa, aquele que você é obrigado a colocar no instagram antes de comer!{Environment.NewLine}Escreve #FKBurguer ae.. #FreeMerchan =)",
                Imagem = "Special.png"
            });

            CardapioCollection = new ObservableCollection<AgrupamentoCardapioModel>();
            CardapioCollection.Add(tradicionaisGrupo);
            CardapioCollection.Add(vegetarianoGrupo);
            CardapioCollection.Add(premiumGrupo);

            CardapioListView.ItemsSource = CardapioCollection;
        }
    }
}
