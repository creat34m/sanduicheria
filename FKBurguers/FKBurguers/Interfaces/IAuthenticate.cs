﻿using Microsoft.WindowsAzure.MobileServices;
using System.Threading.Tasks;

namespace FKBurguers.Interfaces
{
  public interface IAuthenticate
  {
    Task<MobileServiceUser> Authenticate(MobileServiceClient client, MobileServiceAuthenticationProvider provider);
  }
}
