﻿
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;

namespace FKBurguers.Models
{
    public class Cardapio
    {
        // precisa de vc não, se fudeu.. tchau public string Id { get; set; }
        public string Nome { get; set; }
        public string Descricao { get; set; }
        public double Preco { get; set; }
        public string Imagem { get; set; }
    }

    public class AgrupamentoCardapioModel : ObservableCollection<Cardapio>
    {
        public string NomeCompleto { get; set; }
        public string NomeAbreviado { get; set; }
    }
}
