﻿using FKBurguers.Interfaces;
using Microsoft.WindowsAzure.MobileServices;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace FKBurguers.Services
{
  public class AzureService
  {
    public MobileServiceClient Client { get; set; } = null;

    public void Initialize()
    {
      Client = new MobileServiceClient(Constants.AppUrl);
    }

    public async Task<MobileServiceUser> LoginAsync()
    {
      Initialize();

      var auth = DependencyService.Get<IAuthenticate>();
      var user = await auth.Authenticate(Client, MobileServiceAuthenticationProvider.Facebook);

      if (user == null)
      {
        Device.BeginInvokeOnMainThread(async () =>
        {
          await App.Current.MainPage.DisplayAlert("Oops...", "Erro ao autenticar no Facebook", "OK");
        });
        return null;
      }

      return user;
    }
  }
}
