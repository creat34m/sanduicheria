﻿using System;
using Xamarin.Forms;

namespace FKBurguers.Styles
{
    public static class Fonts
    {

        public static string FontAwesome
        {
            get
            {
                string ret = string.Empty;
                switch (Device.RuntimePlatform)
                {
                    case "iOS":
                        ret = "FontAwesome";
                        break;
                    default:
                        ret = "fontawesome.ttf";
                        break;

                }
                return ret;
            }
        }
    }
}
