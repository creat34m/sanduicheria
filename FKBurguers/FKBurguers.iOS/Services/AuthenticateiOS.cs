﻿using System;
using FKBurguers.Interfaces;
using FKBurguers.iOS.Services;
using Microsoft.WindowsAzure.MobileServices;
using System.Threading.Tasks;

[assembly: Xamarin.Forms.Dependency(typeof(AuthenticateiOS))]
namespace FKBurguers.iOS.Services
{
  public class AuthenticateiOS : IAuthenticate
  {
    public async Task<MobileServiceUser> Authenticate(MobileServiceClient client, MobileServiceAuthenticationProvider provider)
    {
      try
      {
        return await client.LoginAsync(UIKit.UIApplication.SharedApplication.KeyWindow.RootViewController, provider);
      }
      catch (Exception ex)
      {
        return null;
      }
    }
  }
}
